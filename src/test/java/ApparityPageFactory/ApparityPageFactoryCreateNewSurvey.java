package ApparityPageFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.validator.PublicClassValidator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApparityPageFactoryCreateNewSurvey {
	
	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	Date date = new Date();
	
	
	String ComplianceSurveyName = "Test Survey"+" "+formatter.format(date);
	
	@FindBy(xpath = "//h4[contains(text(),'Create New Survey')]")
	@CacheLookup
	WebElement txt_CreateNewSurvey;
	
	@FindBy(xpath = "//input[@placeholder = \"Enter Survey Name\"]")
	@CacheLookup
	WebElement input_ComplianceSurveyName;
	
	@FindBy(xpath = "//textarea[@formcontrolname = \"description\"]")
	@CacheLookup
	WebElement txtarea_Description;
	
	@FindBy(xpath = "(//span[@class = \"k-icon k-i-calendar\"])[1]")
	@CacheLookup
	WebElement btn_StartDateAndTimeCalender;
	
	@FindBy(xpath = "//td[@title = \"Wednesday, February 3, 2021\"]")
	@CacheLookup
	WebElement btn_CalenderStartDate;
	
	@FindBy(xpath = "//button[@title = \"Set\"]")
	@CacheLookup
	WebElement btn_CalenderStartDateSet;
	
	@FindBy(xpath = "(//span[@class = \"k-icon k-i-calendar\"])[2]")
	@CacheLookup
	WebElement btn_EndDateAndTimeCalender;
	
	@FindBy(xpath = "//td[@title = \"Sunday, February 28, 2021\"]")
	@CacheLookup
	WebElement btn_CalenderEndDate;
	
	@FindBy(xpath = "//button[@title = \"Set\"]")
	@CacheLookup
	WebElement btn_CalenderEndDateSet;
	
	@FindBy(xpath = "(//span[@class = \"mat-button-wrapper\"])[4]")
	@CacheLookup
	WebElement btn_Apply;
	
	@FindBy(xpath = "(//span[@class = \"mat-button-wrapper\"])[5]")
	@CacheLookup
	WebElement btn_Browse;
	
	WebDriver driver;
	
	public ApparityPageFactoryCreateNewSurvey(WebDriver driver) {
		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 30);
		PageFactory.initElements(factory, this);
	}
	
	public void isCreateNewSurveyLink() {
		txt_CreateNewSurvey.isDisplayed();
	}
	
	public void enterComplianceSurveyName() {
		input_ComplianceSurveyName.sendKeys(ComplianceSurveyName);
	}
	
	public void enterDescription() {
		txtarea_Description.sendKeys("Rishabh100");
	}
	
	public void enterStartAndEndDate() {
		btn_StartDateAndTimeCalender.click();
		btn_CalenderStartDate.click();
		btn_CalenderStartDateSet.click();
		btn_EndDateAndTimeCalender.click();
		btn_CalenderEndDate.click();
		btn_CalenderEndDateSet.click();
		
	}
	
	public void clickApplyButton() {
		btn_Apply.click();
	}
	
	public void clickBrowseButton() throws InterruptedException, IOException {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", btn_Browse);
		Thread.sleep(5000);
//		btn_Browse.sendKeys("C:\\Users\\rishabhsikka\\Downloads\\default-affirmation-survey-2020-12-14_04-54-50.zip");
		Runtime.getRuntime().exec("D:\\git\\AutoIt\\FileUpload.exe");
	}
}

