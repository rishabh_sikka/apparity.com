package ApparityPageFactory;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ApparityPageFactoryHomepage {

	@FindBy(xpath  = "/html/body/div[2]/div/div[1]/div[1]/div[1]/label")
	@CacheLookup
	WebElement txt_InventoryListing;


	@FindBy(xpath = "//div[contains(text(),'Managed / Registered')]")
	@CacheLookup
	WebElement Managed_Registered_Tab;
	
	@FindBy(xpath = "//div[contains(text(),'Unregistered')]")
	@CacheLookup
	WebElement Unregistered_Tab;



	WebDriver driver;
	
	public ApparityPageFactoryHomepage(WebDriver driver) {

		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 50);
		PageFactory.initElements(factory, this);
	}

	public void isInventoryListingLogo() {
		txt_InventoryListing.isDisplayed();
	}
	
	public void Managed_Registered_Tab() 
	{
		Managed_Registered_Tab.click();
	}
	
	public void Unregistered_Tab() 
	{
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", Unregistered_Tab);
		
	}


}
