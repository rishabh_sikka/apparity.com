package ApparityPageFactory;

import java.util.concurrent.Executor;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApparityPageFactoryLeftSideBar {
	
	WebDriver driver;
	
	@FindBy(xpath = "//*[@title=\"Administration\"]")
	@CacheLookup
	WebElement btn_inventory;
	
	@FindBy(xpath = "//*[@title=\"Insights\"]")
	@CacheLookup
	WebElement btn_insights;
	
	@FindBy(xpath = "//div[4]//md-toolbar[1]//div//h5//span//img")
	@CacheLookup
	WebElement btn_survey;	
	
	@FindBy(xpath = "//*[@title=\"Administration\"]")
	@CacheLookup
	WebElement btn_administration;	
	
	@FindBy(xpath = "//*[@title=\"Help & Support\"]")
	@CacheLookup
	WebElement btn_helpSupport;
	
	@FindBy(xpath = "//*[@title=\"About\"]")
	@CacheLookup
	WebElement btn_about;
	
	
	
	public ApparityPageFactoryLeftSideBar(WebDriver driver) {
		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 50);
		PageFactory.initElements(factory, this);
	}
	
	public void clickInventoryButton() {
		
		btn_inventory.click();
	}
	
	public void clickInsightsButton() {
		btn_insights.click();
	}
	
	public void clickSurveyButton() {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", btn_survey);
	}
	
	public  void clickAdministrationButton() {
		btn_administration.click();
	}
	
	public void clickHelpAndSupport() {
		btn_helpSupport.click();
	}
	
	public void clickAboutButton() {
		btn_about.click();
	}

}
