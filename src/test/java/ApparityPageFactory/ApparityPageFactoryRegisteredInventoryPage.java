package ApparityPageFactory;

import java.sql.Driver;
import java.time.Duration;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;

public class ApparityPageFactoryRegisteredInventoryPage {

	WebDriver driver;
	
	List<String> OriginalDataText;
	
	@FindBy(xpath = "//*[@data-title='Application Name']")
	@CacheLookup
	WebElement ApplicationNameColumnHeading;

	@FindBy(xpath = "//div[@id='inventoryGrid']//td[@data-field='name']")
	@CacheLookup
	List<WebElement> OriginalData;

	@FindBy(xpath = "//div[@id='inventoryGrid']//td[@data-field='name']")
	@CacheLookup
	List<WebElement> SortedData;

	@FindBy(xpath = "(//a[@title = 'Edit Column Settings'])[1]")
	@CacheLookup
	WebElement ApplicatioNameContextMenu;

	@FindBy(xpath = "//*[contains(@class,'filter-item')]")
	@CacheLookup
	WebElement ApplicatioNameContextMenuFilter;

	@FindBy(xpath = "//*[contains(text(),'Show items with value that:')]")
	@CacheLookup
	WebElement FilterBox;

	@FindBy (xpath = "//*[contains(@class,'textbox')]")
	@CacheLookup
	WebElement ApplicatioNameContextMenuFilterTextBox;

	@FindBy(xpath = "//*[contains(@class,'k-button k-primary')]")
	@CacheLookup
	WebElement ApplicatioNameContextMenuFilterSubmitButton;

	@FindBy (xpath = "//div[@id='inventoryGrid']//td[@data-field='name']")
	@CacheLookup
	List<WebElement> ApplicationNameFiteredData;
	
	

	public ApparityPageFactoryRegisteredInventoryPage(WebDriver driver) {
		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 10);
		PageFactory.initElements(factory, this);
		
	}

	public void clickApplicatioNameContextMenu() {
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", ApplicatioNameContextMenu);

	}

	public void mouseHoverApplicatioNameContextMenuFilterOption() throws InterruptedException {
		Thread.sleep(1000);

				Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
						.withTimeout(Duration.ofSeconds(30))
						.pollingEvery(Duration.ofSeconds(3))
						.ignoring(NoSuchElementException.class);
				WebElement elementApplicatioNameContextMenuFilter = wait.until(new Function<WebDriver, WebElement>() {
		
					@Override
					public WebElement apply(WebDriver driver) {
		
						if (ApplicatioNameContextMenuFilter.isDisplayed()) {
		
							return ApplicatioNameContextMenuFilter;
		
						}
		
						return null;
					}
		
				});
		Actions MouseHover = new Actions(driver);
		MouseHover.moveToElement(ApplicatioNameContextMenuFilter).perform();
	}

	public void isDisplayedFilterBox() throws InterruptedException {
		Thread.sleep(1000);
				Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
						.withTimeout(Duration.ofSeconds(30))
						.pollingEvery(Duration.ofSeconds(1))
						.ignoring(NoSuchElementException.class);
				WebElement elementFilterBox = wait.until(new Function<WebDriver, WebElement>() {
		
					@Override
					public WebElement apply(WebDriver driver) {
		
						if (FilterBox.isDisplayed()) {
		
							return FilterBox;
		
						}
		
						return null;
					}
		
				});

		System.out.println(FilterBox.isDisplayed());
	}

	public void entersFilterText(String text) {

		ApplicatioNameContextMenuFilterTextBox.sendKeys(text);
	}

	public void clicksFilterButton() {
		ApplicatioNameContextMenuFilterSubmitButton.click();
	}
	public void filteredData() throws InterruptedException	{
		Thread.sleep(1000);
				Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
						.withTimeout(Duration.ofSeconds(30))
						.pollingEvery(Duration.ofMillis(3000))
						.ignoring(NoSuchElementException.class);
				WebElement elementApplicationNameColumnHeading = wait.until(new Function<WebDriver, WebElement>() {
		
					@Override
					public WebElement apply(WebDriver driver) {
		
						if (ApplicationNameColumnHeading.isEnabled()) {
		
							return ApplicationNameColumnHeading;
		
						}
		
						return null;
					}
		
				});

		for(WebElement e : ApplicationNameFiteredData) {
			boolean flag = e.getText().toLowerCase().contains("test");
			if(flag == false) {
				Assert.assertEquals(true, flag);
			}
			//			System.out.println(e.getText());

		}


	}

	public void clickApplicatioNameColumnHeading() throws InterruptedException {
		Thread.sleep(1000);
		List<String> OriginalDataText=ApplicationNameFiteredData.stream().map(o->o.getText()).collect(Collectors.toList());
		Collections.sort(OriginalDataText, String.CASE_INSENSITIVE_ORDER);
		for(String st : OriginalDataText) {
			System.out.println(st);
		}

		boolean  flagcoumn = ApplicationNameColumnHeading.isDisplayed();
		//		System.out.println(flagcoumn);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", ApplicationNameColumnHeading);
		
		Thread.sleep(1000);
//		List<String> OriginalDataText = ApplicationNameFiteredData.stream().map(o->o.getText()).collect(Collectors.toList());
//		Thread.sleep(4000);
		List<String>SortedDatatext = SortedData.stream().map(s->s.getText()).collect(Collectors.toList());
		for(String st1 : SortedDatatext) {
			System.out.println(st1);
		}

		boolean isEqual = OriginalDataText.equals(SortedDatatext);
		System.out.println(isEqual);

	}

	public void sortedData() throws InterruptedException {
		Thread.sleep(3000);
//		List<String> OriginalDataText = ApplicationNameFiteredData.stream().map(o->o.getText()).collect(Collectors.toList());
//		Thread.sleep(4000);
		List<String>SortedDatatext = SortedData.stream().map(s->s.getText()).collect(Collectors.toList());
		for(String st1 : SortedDatatext) {
			System.out.println(st1);
		}
		boolean isEqual = OriginalDataText.equals(SortedDatatext);
		System.out.println(isEqual);

	}


}


