package ApparityPageFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

public class ApparityPageFactoryUnregisteredGrouping 
{
	WebDriver driver;

	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	Date date = new Date();
	String NewGroupName = "TestGroup"+" "+formatter.format(date);

	@FindBy(xpath = "//span[contains(text(),'Set Location Preference')]")
	@CacheLookup
	WebElement Set_Location_Prefe_Btn;

	@FindBy(xpath = "(//md-checkbox[@class='unregExcelGroupSelectCheck ng-scope'])[1]")
	@CacheLookup
	WebElement Group_checkBox;

	@FindBy(xpath="//span[contains(text(),'Change Group')]")
	@CacheLookup
	WebElement Change_Group_Btn;

	@FindBy(xpath = "//div[contains(text(),'Change Groups')]")
	@CacheLookup
	WebElement ChangeGroupPanel;

	@FindBy(xpath = "//md-radio-button[@id='radio_2']")
	@CacheLookup
	WebElement New_Group_Radio_Btn;

	@FindBy(xpath = "//input[@id='newGroupNameInput']")
	@CacheLookup
	WebElement New_Group_Text_Box;

	@FindBy(xpath = "(//span[contains(text(),'Change')])[1]")
	@CacheLookup
	WebElement Pop_Up_Change_Group_Btn;

	@FindBy(xpath="(//span[contains(text(),'Cancel')])[1]")
	@CacheLookup
	WebElement Pop_Up_Cancel_Btn;

	@FindBy(xpath = "//div[contains(text(),'Your changes were applied successfully')]")
	@CacheLookup
	WebElement Group_Change_Success_Message;

	@FindBy(xpath = "//span[contains(text(),'Close')]")
	@CacheLookup
	WebElement Success_Message_Close_Btn;

	@FindBy(xpath = "(//*[@title=\"Edit Column Settings\"])[17]")
	@CacheLookup
	WebElement GroupNameContextMenu;

	@FindBy(xpath = "//*[contains(@class,'filter-item')]")
	@CacheLookup
	WebElement GroupNameContextMenuFilter;

	@FindBy(xpath = "//*[contains(@class,'textbox')]")
	@CacheLookup
	WebElement GroupNameContextMenuFilterTextBox;
	
	@FindBy(xpath = "//*[contains(@class,'k-button k-primary')]")
	@CacheLookup
	WebElement GroupNameContextMenuFilterSubmitButton;
	
	@FindBy(xpath = "//span[contains(text(),'TestGroup')]")
	@CacheLookup
	WebElement LatestGroupName;


	public ApparityPageFactoryUnregisteredGrouping(WebDriver driver) {

		this.driver = driver;
		AjaxElementLocatorFactory factory = new AjaxElementLocatorFactory(driver, 50);
		PageFactory.initElements(factory, this);

	}

	public void Group_Name_CheckBox() throws InterruptedException
	{
//		Thread.sleep(5000);
		Group_checkBox.click();

	}

	public void Set_Location_button() throws InterruptedException 
	{
//		Thread.sleep(5000);
		boolean location_btn_displayed = Set_Location_Prefe_Btn.isDisplayed();	
		System.out.println(location_btn_displayed);
	}

	public void  displayChangeGroupBtn() throws InterruptedException {
		Change_Group_Btn.isDisplayed();
		Change_Group_Btn.isEnabled();
	}

	public void clickChangeGroupBtn() {
		Change_Group_Btn.click();

	}

	public void isDisplayedChangeGroupPanel() throws InterruptedException {
		Thread.sleep(2000);
		boolean flag = ChangeGroupPanel.isDisplayed();
		System.out.println(flag);

	}

	public void enterNewGroupName() {

		New_Group_Text_Box.sendKeys(NewGroupName);

	}

	public void isDisplayedChangeGroupPanelChangeBtn() {
		Pop_Up_Change_Group_Btn.isEnabled();
	}

	public void clickChangeGroupPanelChangeBtn() {

		Pop_Up_Change_Group_Btn.click();
	}

	public void groupChangeSuccessmassage() {

		boolean SuccessMessageFlag = Group_Change_Success_Message.isDisplayed();
		String SuccessMessage = Group_Change_Success_Message.getText();
		Assert.assertEquals("Your changes were applied successfully", SuccessMessage );
	}

	public void clickChangeGroupPanelCloseBtn() {

		Success_Message_Close_Btn.click();

	}

	public void clickGroupNameContextMenuBtn() throws InterruptedException  {
		Thread.sleep(3000);
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", GroupNameContextMenu);
	}

	public void mouseHoverGroupNameContextMenuFilterOption() throws InterruptedException {
		Thread.sleep(3000);
		Actions MouseHover = new Actions(driver);
		MouseHover.moveToElement(GroupNameContextMenuFilter).perform();
	}

	public void enterGroupNameFilterText() {
		GroupNameContextMenuFilterTextBox.sendKeys(NewGroupName);

	}
	
	public void clickGroupNameFilterBtn() {
		
		GroupNameContextMenuFilterSubmitButton.click();
		
	}
	
	public void filterLatestGroupName() {
	Boolean flagLatestGroupName	= LatestGroupName.isDisplayed();
	System.out.println(flagLatestGroupName);
	Assert.assertEquals(true, flagLatestGroupName);
		
	}


}
