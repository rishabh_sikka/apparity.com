package ApparityStepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import ApparityPageFactory.ApparityPageFactoryHomepage;
import ApparityPageFactory.ApparityPageFactoryLoginPage;
import ApparityPageFactory.ApparityPageFactoryRegisteredInventoryPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en_lol.AN;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ApparityInventorySorting {
	
	WebDriver driver;
	ApparityPageFactoryLoginPage Login;
	ApparityPageFactoryHomepage Homepage;
	ApparityPageFactoryRegisteredInventoryPage InventoryPage;
	
	@Given("User is present on login page for sorting")
	public void user_is_present_on_login_page_for_sorting() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.setAcceptInsecureCerts(true);
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://10.127.130.110/");
	}

	@And("User enters UserName {string} and Pwd {string} for sorting")
	public void user_enters_UserName_and_Pwd_for_sorting(String username, String password) {
		Login = new ApparityPageFactoryLoginPage(driver);
		Login.enterUsername(username);
		Login.enterPassword(password);
	}

	@And("User clicks on Sign In button for sorting")
	public void user_clicks_on_Sign_In_button_for_sorting() {
		Login.clickSubmitButton();
	}

	@Then("User naviagtes to home page for sorting")
	public void user_naviagtes_to_home_page_for_sorting() {
		Homepage = new ApparityPageFactoryHomepage(driver);
		Homepage.isInventoryListingLogo();
	}

	@When("User clicks on context menu icon in Application name column for sorting")
	public void user_clicks_on_context_menu_icon_in_Application_name_column_for_sorting() {
		InventoryPage = new ApparityPageFactoryRegisteredInventoryPage(driver);
		InventoryPage.clickApplicatioNameContextMenu();		
	}
	
	@And("User hovers on filter option for sorting")
	public void user_hovers_on_filter_option_for_sorting() throws InterruptedException {
	    InventoryPage.mouseHoverApplicatioNameContextMenuFilterOption();
	}
	
	@Then("Filter option should expand for sorting")
	public void filter_option_should_expand_for_sorting() throws InterruptedException {
	   InventoryPage.isDisplayedFilterBox();
	}
	
	@When("User enters filter text {string} for sorting")
	public void user_enters_filter_text_for_sorting(String text) {
	    InventoryPage.entersFilterText(text);
	}
	
	@And("Clicks on filter button for sorting")
	public void clicks_on_filter_button_for_sorting() {
	    InventoryPage.clicksFilterButton();
	}

	@Then("All the results containg the filter text should appear for sorting")
	public void all_the_results_containg_the_filter_text_should_appear_for_sorting() throws InterruptedException {
	    InventoryPage.filteredData();
	}
	
	@And("User clicks on Application Name column heading for sorting")
	public void user_clicks_on_Application_Name_column_heading_for_sorting() throws InterruptedException {
		InventoryPage.clickApplicatioNameColumnHeading();
	}

	@Then("Filtered data should be sorted in ascending order for sorting")
	public void filtered_data_should_be_sorted_in_ascending_order_for_sorting() throws InterruptedException {
	    System.out.println("Application Name column is sorted sucessfully");
	}

}
