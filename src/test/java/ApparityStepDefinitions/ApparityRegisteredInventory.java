package ApparityStepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import ApparityPageFactory.ApparityPageFactoryHomepage;
import ApparityPageFactory.ApparityPageFactoryLoginPage;
import ApparityPageFactory.ApparityPageFactoryRegisteredInventoryPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ApparityRegisteredInventory {

	WebDriver driver;
	ApparityPageFactoryLoginPage Login;
	ApparityPageFactoryHomepage Homepage;
	ApparityPageFactoryRegisteredInventoryPage InventoryPage;

	@Given("User is present on login page")
	public void user_is_present_on_login_page() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.setAcceptInsecureCerts(true);
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://10.127.130.110/");
	}

	@And("User enters UserName {string} and Pwd {string}")
	public void user_enters_UserName_and_Pwd(String username, String password) {
		Login = new ApparityPageFactoryLoginPage(driver);
		Login.enterUsername(username);
		Login.enterPassword(password);

	}
	@And("User clicks on Sign In button")
	public void user_clicks_on_Sign_In_button() {
		Login.clickSubmitButton();
	}

	@Then("User naviagtes to home page")
	public void user_naviagtes_to_home_page() {
		Homepage = new ApparityPageFactoryHomepage(driver);
		Homepage.isInventoryListingLogo();
	}

	@When("User clicks on context menu icon in Application name column")
	public void user_clicks_on_context_menu_icon_in_Application_name_column() {
		InventoryPage = new ApparityPageFactoryRegisteredInventoryPage(driver);
		InventoryPage.clickApplicatioNameContextMenu();
	}

	@And("User hovers on filter option")
	public void user_hovers_on_filter_option() throws InterruptedException {
		InventoryPage.mouseHoverApplicatioNameContextMenuFilterOption();
	}

	@Then("Filter option should expand")
	public void filter_option_should_expand() throws InterruptedException {
		InventoryPage.isDisplayedFilterBox();
	}

	@When("User enters filter text {string}")
	public void user_enters_filter_text(String text) {
		InventoryPage.entersFilterText(text);
	}

	@And("Clicks on filter button")
	public void clicks_on_filter_button() {
		InventoryPage.clicksFilterButton();
	}

	@Then("All the results containg the filter text should appear")
	public void all_the_results_containg_the_filter_text_should_appear() throws InterruptedException   {
		InventoryPage.filteredData();
		driver.close();
	}
}
