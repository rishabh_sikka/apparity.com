package ApparityStepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import ApparityPageFactory.ApparityPageFactoryHomepage;
import ApparityPageFactory.ApparityPageFactoryLoginPage;
import ApparityPageFactory.ApparityPageFactoryRegisteredInventoryPage;
import ApparityPageFactory.ApparityPageFactoryUnregisteredGrouping;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.github.bonigarcia.wdm.WebDriverManager;

public class ApparityUnregistedGrouping {
	
	WebDriver driver;
	ApparityPageFactoryLoginPage Login;
	ApparityPageFactoryHomepage Homepage;
	ApparityPageFactoryRegisteredInventoryPage InventoryPage;
	ApparityPageFactoryUnregisteredGrouping Grouping;

	@Given("User is available on login page")
	public void user_is_available_on_login_page() {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.setAcceptInsecureCerts(true);
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://10.127.130.110/");
	}

	@And("User enters credentails UserName {string} and Pwd {string}")
	public void user_enters_credentails_UserName_and_Pwd(String username, String password)
	{

		Login = new ApparityPageFactoryLoginPage(driver);
		Login.enterUsername(username);
		Login.enterPassword(password);
	}

	@And("User clicks on Sign-In button")
	public void user_clicks_on_Sign_In_button() {
		Login.clickSubmitButton();
	}

	@Then("User naviagte to home page")
	public void user_naviagte_to_home_page() {
		Homepage = new ApparityPageFactoryHomepage(driver);
		Homepage.isInventoryListingLogo();
	}

	@And("User clicks on the unregistered tab")
	public void user_clicks_on_the_unregistered_tab() throws InterruptedException {
//		Thread.sleep(3000);
		Homepage.Unregistered_Tab();
	}

	@Then("User navigates to the unregistered grid")
	public void user_navigates_to_the_unregistered_grid() throws InterruptedException {
		Grouping=new ApparityPageFactoryUnregisteredGrouping(driver);
		Grouping.Set_Location_button();
	}

	@And("User selects the Existing group")
	public void user_selects_the_Existing_group() throws InterruptedException {

		Grouping.Group_Name_CheckBox();
	}

	@Then("Change group button should displayed")
	public void change_group_button_should_displayed() throws InterruptedException {

		Grouping.displayChangeGroupBtn();
	}

	@And("User clicks on the Change group button")
	public void user_clicks_on_the_Change_group_button() {
		Grouping.clickChangeGroupBtn();

	}

	@Then("Change group pop up should displayed")
	public void change_group_pop_up_should_displayed() throws InterruptedException {
		Grouping.isDisplayedChangeGroupPanel();

	}

	@And("User enters the new group name")
	public void user_enters_the_new_group_name() {
		Grouping.enterNewGroupName();
	}

	@Then("Change button should enabled")
	public void change_button_should_enabled() {
		Grouping.isDisplayedChangeGroupPanelChangeBtn();
	}

	@And("User clicks on the change group")
	public void user_clicks_on_the_change_group() {
		Grouping.clickChangeGroupPanelChangeBtn();
		Grouping.groupChangeSuccessmassage();
		Grouping.clickChangeGroupPanelCloseBtn();
	}

	@Then("New group should created")
	public void new_group_should_created() throws InterruptedException {
		Grouping.clickGroupNameContextMenuBtn();
		Grouping.mouseHoverGroupNameContextMenuFilterOption();
		Grouping.enterGroupNameFilterText();
		Grouping.clickGroupNameFilterBtn();
		Grouping.filterLatestGroupName();

	}

}
