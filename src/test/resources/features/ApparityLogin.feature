Feature: User is able to login into Apparity successfully

  Scenario Outline: User is able to successfully login to Apparity
    Given sample browser is open successfully
    And sample User enters URL and SSL is bypassed successfully
    When User enters username <Username> and  User enters password <Password>
    And sample Clicks on login button
    Then sample User is navigated to the homepage

    Examples:

      | Username       | Password     |
      | "Apparity_admin" | "Apparity@123" |
      |"rishabh"|"Apparity@123"|