$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/ApparityLogin.feature");
formatter.feature({
  "name": "User is able to login into Apparity successfully",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "sample browser is open successfully",
  "keyword": "Given "
});
formatter.step({
  "name": "sample User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.step({
  "name": "User enters username \u003cUsername\u003e and  User enters password \u003cPassword\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "sample Clicks on login button",
  "keyword": "And "
});
formatter.step({
  "name": "sample User is navigated to the homepage",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "Username",
        "Password"
      ]
    },
    {
      "cells": [
        "\"Apparity_admin\"",
        "\"Apparity@123\""
      ]
    },
    {
      "cells": [
        "\"rishabh\"",
        "\"Apparity@123\""
      ]
    }
  ]
});
formatter.scenario({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "sample browser is open successfully",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_browser_is_open_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_User_enters_URL_and_SSL_is_bypassed_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters username \"Apparity_admin\" and  User enters password \"Apparity@123\"",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.user_enters_username_and_User_enters_password(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample Clicks on login button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_Clicks_on_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User is navigated to the homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_User_is_navigated_to_the_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "User is able to successfully login to Apparity",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "sample browser is open successfully",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_browser_is_open_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User enters URL and SSL is bypassed successfully",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_User_enters_URL_and_SSL_is_bypassed_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters username \"rishabh\" and  User enters password \"Apparity@123\"",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.user_enters_username_and_User_enters_password(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample Clicks on login button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_Clicks_on_login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "sample User is navigated to the homepage",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityLogin.sample_User_is_navigated_to_the_homepage()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/Grouping.feature");
formatter.feature({
  "name": "User is able to perform grouping in the Unregistered grid.",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User is able to move the Existing group to New Group",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User is available on login page",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_is_available_on_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters credentails UserName \"Apparity_Admin\" and Pwd \"Apparity@123\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_enters_credentails_UserName_and_Pwd(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Sign-In button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_clicks_on_Sign_In_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User naviagte to home page",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_naviagte_to_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the unregistered tab",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_clicks_on_the_unregistered_tab()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User navigates to the unregistered grid",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_navigates_to_the_unregistered_grid()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User selects the Existing group",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_selects_the_Existing_group()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Change group button should displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.change_group_button_should_displayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the Change group button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_clicks_on_the_Change_group_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Change group pop up should displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.change_group_pop_up_should_displayed()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters the new group name",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_enters_the_new_group_name()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Change button should enabled",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.change_button_should_enabled()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on the change group",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.user_clicks_on_the_change_group()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "New group should created",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityUnregistedGrouping.new_group_should_created()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/Inventory.feature");
formatter.feature({
  "name": "User is able to perform the basic operations on inventory screen",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "User is able to perform the filter Application Name in Registered screen",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User is present on login page",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_is_present_on_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters UserName \"Apparity_Admin\" and Pwd \"Apparity@123\"",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_enters_UserName_and_Pwd(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Sign In button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_clicks_on_Sign_In_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User naviagtes to home page",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_naviagtes_to_home_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on context menu icon in Application name column",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_clicks_on_context_menu_icon_in_Application_name_column()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User hovers on filter option",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_hovers_on_filter_option()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Filter option should expand",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.filter_option_should_expand()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters filter text \"test\"",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.user_enters_filter_text(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Clicks on filter button",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.clicks_on_filter_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "All the results containg the filter text should appear",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityRegisteredInventory.all_the_results_containg_the_filter_text_should_appear()"
});
formatter.result({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/InvnetorySorting.feature");
formatter.feature({
  "name": "inventory sorting functionality",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "user is able to sort application name column",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "User is present on login page for sorting",
  "keyword": "Given "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_is_present_on_login_page_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters UserName \"Apparity_Admin\" and Pwd \"Apparity@123\" for sorting",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_enters_UserName_and_Pwd_for_sorting(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Sign In button for sorting",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_clicks_on_Sign_In_button_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User naviagtes to home page for sorting",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_naviagtes_to_home_page_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on context menu icon in Application name column for sorting",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_clicks_on_context_menu_icon_in_Application_name_column_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User hovers on filter option for sorting",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_hovers_on_filter_option_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Filter option should expand for sorting",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.filter_option_should_expand_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User enters filter text \"test\" for sorting",
  "keyword": "When "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_enters_filter_text_for_sorting(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Clicks on filter button for sorting",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.clicks_on_filter_button_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "All the results containg the filter text should appear for sorting",
  "keyword": "Then "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.all_the_results_containg_the_filter_text_should_appear_for_sorting()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User clicks on Application Name column heading for sorting",
  "keyword": "And "
});
formatter.match({
  "location": "ApparityStepDefinitions.ApparityInventorySorting.user_clicks_on_Application_Name_column_heading_for_sorting()"
});
formatter.result({
  "status": "passed"
});
});