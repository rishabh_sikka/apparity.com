Feature: User is able to perform grouping in the Unregistered grid.

Scenario: User is able to move the Existing group to New Group
Given User is available on login page
And  User enters credentails UserName "Apparity_Admin" and Pwd "Apparity@123"
And User clicks on Sign-In button
Then User naviagte to home page
And User clicks on the unregistered tab
Then User navigates to the unregistered grid
And User selects the Existing group
Then Change group button should displayed 
And User clicks on the Change group button
Then Change group pop up should displayed
And User enters the new group name
Then Change button should enabled
And User clicks on the change group
Then New group should created