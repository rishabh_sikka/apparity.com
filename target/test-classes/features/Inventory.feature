Feature: User is able to perform the basic operations on inventory screen

  Scenario: User is able to perform the filter Application Name in Registered screen
    Given User is present on login page
    And User enters UserName "Apparity_Admin" and Pwd "Apparity@123"
    And User clicks on Sign In button
    Then User naviagtes to home page
    When User clicks on context menu icon in Application name column
    And User hovers on filter option
    Then Filter option should expand
    When User enters filter text "test"
    And Clicks on filter button
    Then All the results containg the filter text should appear