Feature: inventory sorting functionality

  Scenario: user is able to sort application name column
    Given User is present on login page for sorting
    And User enters UserName "Apparity_Admin" and Pwd "Apparity@123" for sorting
    And User clicks on Sign In button for sorting
    Then User naviagtes to home page for sorting
    When User clicks on context menu icon in Application name column for sorting
    And User hovers on filter option for sorting
    Then Filter option should expand for sorting
    When User enters filter text "test" for sorting
    And Clicks on filter button for sorting
    Then All the results containg the filter text should appear for sorting
    And User clicks on Application Name column heading for sorting
    Then Filtered data should be sorted in ascending order for sorting